# Just Record

Records audio using Apple framework, then uses Google Cloud Speech API's to get the transcribed audio for later use with Google Translate API in order to translate from the detected language to spanish.

# Architecture 

Using MVVM for view layer and simple stack for the rest of the application, networking layer using Moya and asyncronicity using RxSwift.

# GIF

![GIF](https://i.imgur.com/SZtdErV.gif)

 