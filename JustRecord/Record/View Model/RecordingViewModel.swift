import RxSwift
import RxCocoa

private typealias ViewModel = RecordingViewModelType & RecordingViewModelInput & RecordingViewModelOutput

struct RecordingViewModel: ViewModel {
  
  var input: RecordingViewModelInput { self }
  var output: RecordingViewModelOutput { self }
 
  var state: Driver<RecordingViewState> { stateRelay.asDriver() }
  private var stateRelay = BehaviorRelay<RecordingViewState>(value: .idle)
  
  private let bag = DisposeBag()
  
  // MARK: - Dependencies
  
  private let audioRecordingService: AudioRecordingService
  private let speechToTextService: SpeechToTextService
  private let translationService: TranslationService
  
  init(audioRecordingService: AudioRecordingService = AVFoundationAudioRecordingService(),
       speechToTextService: SpeechToTextService = GCloudSpeechToTextService(),
       translationService: TranslationService = GCloudTranslationService())
  {
    self.audioRecordingService = audioRecordingService
    self.speechToTextService = speechToTextService
    self.translationService = translationService
  }
  
  // MARK: - Input
  
  func onLoad() {
    observeAudioRecording()
  }
  
  func onRecordButtonPressed() {
    guard stateRelay.value.isRecording else {
      audioRecordingService.record()
      return
    }
    audioRecordingService.stop()
    stateRelay.accept(.translating)
  }
  
  private func observeAudioRecording() {
    audioRecordingService.state
    .map(handleAudioRecording)
    .subscribe()
    .disposed(by: bag)
  }
  
  private func handleAudioRecording(_ state: AudioRecordingState) {
    switch state {
    case .idle:
      stateRelay.accept(.idle)
    case .errored(let error):
      stateRelay.accept(.error(error))
    case .finished(let speech):
      evaluateRecording(with: speech)
    case .recording:
      stateRelay.accept(.recording)
    }
  }
  
  private func evaluateRecording(with speech: Data) {
    speechToTextService.evaluate(speech)
      .flatMap(translationService.translate)
      .subscribe(onSuccess: { text in
        self.stateRelay.accept(.translated(text: text))
      }) { error in
        self.stateRelay.accept(.error(error))
    }.disposed(by: bag)
  }
}
