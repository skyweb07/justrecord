import RxSwift
import RxCocoa

protocol RecordingViewModelType {
  var input: RecordingViewModelInput { get }
  var output: RecordingViewModelOutput { get }
}

protocol RecordingViewModelInput {
  func onLoad()
  func onRecordButtonPressed()
}

protocol RecordingViewModelOutput {
  var state: Driver<RecordingViewState> { get }
}
