import Foundation

enum RecordingViewState {
  case idle
  case recording
  case translating
  case translated(text: String)
  case error(Error)
  
  var isRecording: Bool {
    if case RecordingViewState.recording = self { return true }
    return false
  }
}
