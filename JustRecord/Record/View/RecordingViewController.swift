import UIKit
import RxSwift
import RxCocoa

final class RecordingViewController: UIViewController {
  @IBOutlet weak var actionButton: UIButton!
  @IBOutlet weak var statusView: UIView!
  @IBOutlet weak var statusLabel: UILabel!
  @IBOutlet weak var translationStackView: UIStackView!
  @IBOutlet weak var translatedTextLabel: UILabel!
  
  private let viewModel: RecordingViewModelType = RecordingViewModel()
  private let bag = DisposeBag()
 
  override func viewDidLoad() {
    super.viewDidLoad()
    viewModel.input.onLoad()
    setupBindings()
  }
  
  // MARK: - Bindings
  
  private func setupBindings() {
    viewModel.output.state
      .map(handle)
      .asObservable()
      .subscribe()
      .disposed(by: bag)
    
    actionButton.rx.tap
      .debounce(.milliseconds(3), scheduler: MainScheduler.instance)
      .subscribe(onNext: { [weak self] _ in
        self?.viewModel.input.onRecordButtonPressed()
      }).disposed(by: bag)
  }
  
  // MARK: - UI State
  
  private func handle(_ state: RecordingViewState) {
    switch state {
    case .idle:
      configure(isRecording: false)
    case .recording:
      translationStackView.isHidden = true
      configure(isRecording: true)
    case .translating:
      statusLabel.text = "Translating..."
    case .translated(let text):
      translatedTextLabel.text = text
      translationStackView.isHidden = false
      configure(isRecording: false)
    case .error(let error):
      configure(isRecording: false)
      showError(error)
    }
  }
  
  private func configure(isRecording: Bool) {
    let title = isRecording ? "Stop" : "Record"
    actionButton.setTitle(title, for: .normal)
    actionButton.backgroundColor = isRecording ? .lightGray : .red
    statusView.isHidden = !isRecording
  }
}
