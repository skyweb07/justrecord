import Foundation
import UIKit

extension RecordingViewController {
  func showError(_ error: Error) {
    let alertView = UIAlertController(
      title: "Error",
      message: error.localizedDescription,
      preferredStyle: .alert
    )
    let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
    alertView.addAction(okAction)
    
    present(alertView, animated: true, completion: nil)
  }
}
