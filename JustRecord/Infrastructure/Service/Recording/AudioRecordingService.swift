import Foundation
import RxSwift

enum AudioRecordingError: Error {
  case generic(Error)
  case unknown
  case userPermissionDenied
}

enum AudioRecordingState {
  case idle
  case finished(with: Data)
  case errored(AudioRecordingError)
  case recording
}

protocol AudioRecordingService {
  var state: Observable<AudioRecordingState> { get }
  
  func record()
  func stop()
}
