import RxSwift
import AVFoundation

final class AVFoundationAudioRecordingService: NSObject, AudioRecordingService {
  
  var state: Observable<AudioRecordingState> {
    return stateRelay.asObservable()
  }
  fileprivate var stateRelay = BehaviorSubject<AudioRecordingState>(value: .idle)
  
  private let recordingSession: AVAudioSession
  private var audioRecorder: AVAudioRecorder?
  private let fileManager: FileManager
 
  init(recordingSession: AVAudioSession = .sharedInstance(),
       fileManager: FileManager = .default)
  {
    self.recordingSession = recordingSession
    self.fileManager = fileManager
  }
  
  func record() {
    setupRecordingSession { [weak self] recordingAllowed in
      guard recordingAllowed else {
        self?.stateRelay.onNext(.errored(.userPermissionDenied))
        return
      }
      self?.startRecording()
    }
  }
  
  private func startRecording() {
    guard let recordingUrl = recordingPath else { return }
    
    let audioRecordingSettings = [
      AVFormatIDKey: Int(kAudioFormatFLAC),
      AVSampleRateKey: 16000,
      AVNumberOfChannelsKey: 1,
      AVEncoderAudioQualityKey: AVAudioQuality.max.rawValue
    ]
    
    do {
      try audioRecorder = AVAudioRecorder(url: recordingUrl, settings: audioRecordingSettings)
      audioRecorder?.delegate = self
      
      guard let isRecording = audioRecorder?.record(), isRecording else {
        stateRelay.onNext(.errored(.unknown))
        return
      }
      stateRelay.onNext(.recording)
    } catch {
      stateRelay.onNext(.errored(.generic(error)))
    }
  }
  
  func stop() {
    audioRecorder?.stop()
    audioRecorder = nil
  }
  
  // MARK: - Permission
  
  private func setupRecordingSession(with permissions: @escaping (Bool) -> Void) {
    do {
      try recordingSession.setCategory(.record)
      try recordingSession.setActive(true)
      recordingSession.requestRecordPermission(permissions)
    } catch {
      stateRelay.onNext(.errored(.generic(error)))
    }
  }
  
  // MARK: - Recording path
  
  private var recordingPath: URL?  {
    return fileManager.urls(for: .documentDirectory, in: .userDomainMask)
      .first?
      .appendingPathComponent("recording.caf")
  }
}

// MARK: - AVAudioRecorderDelegate

extension AVFoundationAudioRecordingService: AVAudioRecorderDelegate {
  func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
    guard flag, let recordingUrl = recordingPath else {
      stateRelay.onNext(.errored(.unknown))
      return
    }
 
    do {
      let recordingData = try Data(contentsOf: recordingUrl)
      stateRelay.onNext(.finished(with: recordingData))
    } catch {
      stateRelay.onNext(.errored(.generic(error)))
    }
  }

  func audioRecorderEncodeErrorDidOccur(_ recorder: AVAudioRecorder, error: Error?) {
    guard let error = error else { return }
    stateRelay.onNext(.errored(.generic(error)))
  }
}
