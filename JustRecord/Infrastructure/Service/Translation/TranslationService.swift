import RxSwift

enum TranslationServiceError: Error {
  case generic(Error)
  case noTranslation
}

protocol TranslationService {
  func translate(_ text: String) -> Single<String>
}
