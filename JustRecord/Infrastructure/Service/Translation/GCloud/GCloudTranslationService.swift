import RxSwift
import Moya

struct GCloudTranslationService: TranslationService {
  
  private let apiService: MoyaProvider<GCloudTranslationApiService>
  
  init(apiService: MoyaProvider<GCloudTranslationApiService> = MoyaProvider()) {
    self.apiService = apiService
  }
  
  func translate(_ text: String) -> Single<String> {
    return apiService.rx.request(.translate(with: text))
      .filterSuccessfulStatusCodes()
      .map(GoogleTranslateTextResponseModel.self)
      .map { response in
        guard let translation = response.data.translations.first?.translatedText else {
          throw TranslationServiceError.noTranslation
        }
        return translation
    }
  }
}
