import Moya

private enum Credentials {
  static let apiKey = "AIzaSyAb1hZKMHueQwSFh5JFDuDI_6I06qprAs4"
}

enum GCloudTranslationApiService: TargetType {
  var baseURL: URL {
    return URL(string: "https://translation.googleapis.com/")!
  }
  
  var path: String {
    return "language/translate/v2"
  }
  
  var method: Method { .post }
  var sampleData: Data { Data() }
  
  var task: Task {
    switch self {
    case .translate(let text):
      return .requestParameters(
        parameters: [
          "target": "es",
          "key": Credentials.apiKey,
          "q": text
        ],
        encoding: URLEncoding.default)
    }
  }
  
  var headers: [String : String]? {
    return ["X-Ios-Bundle-Identifier": "in.skydev.JustRecord"]
  }
  
  case translate(with: String)
}
