import Foundation

struct GoogleTranslateTextResponseModel: Codable {
  let data: GoogleTranslationData
}

struct GoogleTranslationData: Codable {
  let translations: [GoogleTranslation]
}

struct GoogleTranslation: Codable {
  let translatedText: String
}
