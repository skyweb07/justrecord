import RxSwift

enum SpeechToTextError: Error {
  case generic(Error)
  case noTranslation
}

protocol SpeechToTextService {
  func evaluate(_ speech: Data) -> Single<String>
}
