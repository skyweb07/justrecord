import RxSwift
import Moya

struct GCloudSpeechToTextService: SpeechToTextService {
   
  private let apiService: MoyaProvider<GCloudSpeechApiService>
  
  init(apiService: MoyaProvider<GCloudSpeechApiService> = MoyaProvider()) {
    self.apiService = apiService
  }
  
  func evaluate(_ speech: Data) -> Single<String> {
    return apiService.rx.request(.speechToText(with: speech))
      .filterSuccessfulStatusCodes()
      .map(GoogleSpeechResponseModel.self)
      .map { response in
        guard let transcript = response.results.first?.alternatives.first?.transcript else {
          throw SpeechToTextError.noTranslation
        }
        return transcript
    }
  }
}
