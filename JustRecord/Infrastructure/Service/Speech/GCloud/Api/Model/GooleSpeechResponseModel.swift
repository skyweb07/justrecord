import Foundation

struct GoogleSpeechResponseModel: Codable {
  let results: [GoogleSpeechResult]
}

struct GoogleSpeechResult: Codable {
  let alternatives: [GoogleSpeechAlternative]
}

struct GoogleSpeechAlternative: Codable {
  let transcript: String
  let confidence: Double
}

