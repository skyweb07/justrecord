import Moya

private enum Credentials {
  static let apiKey = "AIzaSyAb1hZKMHueQwSFh5JFDuDI_6I06qprAs4"
}

enum GCloudSpeechApiService: TargetType {
  var baseURL: URL {
    return URL(string: "https://speech.googleapis.com/v1/")!
  }
  
  var path: String {
    return "speech:recognize"
  }
 
  var method: Method { .post }
  var sampleData: Data { Data() }
  
  var task: Task {
    
    switch self {
    case .speechToText(let speechData):
      
      let configRequest: [String: Any] = [
        "encoding": "FLAC",
        "sampleRateHertz": 16000,
        "languageCode": "en-US",
        "maxAlternatives": 0
      ]

      let audioRequest = [
        "content": speechData.base64EncodedString(options: Data.Base64EncodingOptions.init(rawValue: 0))
      ]

      let parameters: [String: Any] = [
        "config": configRequest,
        "audio": audioRequest
      ]
      
      return .requestCompositeParameters(
        bodyParameters: parameters,
        bodyEncoding: JSONEncoding.default,
        urlParameters: ["key": Credentials.apiKey]
      )
    }
  }
  
  var headers: [String : String]? {
    return ["X-Ios-Bundle-Identifier": "in.skydev.JustRecord"]
  }
 
  case speechToText(with: Data)
}
